/*  Library call  */
const readline = require('readline');

/*  The Product List from OSL provided
    SKU      Name            Price
    ipd      Super iPad      $549.99
    mbp      MacBook Pro     $1399.99
    atv      Apple TV        $109.50
    vga      VGA adapter     $30.00
*/
const SKUProductList = [
    {
        "sku": "ipd",
        "name": "Super iPad",
        "price": 549.99
    },
    {
        "sku": "mbp",
        "name": "MacBook Pro",
        "price": 1399.99
    },
    {
        "sku": "atv",
        "name": "Apple TV",
        "price": 109.5
    },
    {
        "sku": "vga",
        "name": "VGA adapter",
        "price": 30.00
    },
]

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/*  the function of discount is planned to 
    check whether the order can enjoy the discount */
function discount(request, callback) {
    // console.log("-------- DISCOUNT --------")
    // console.log(`Product list: ${request}`);

    //  split it for determinnig the product price
    var skuList = request.split(", ");
    //  default a state var for this func to pack the data of SKUs
    var product = {};
    //  contains the detail of sku scanned
    var productList = [];
    //  global var to record the total number of the atv and ipd
    var atvCount = 0, ipdCount = 0;
    /*  bools are containing the status of mbp and vga to
        check they are exsiting at the same time */
    var hvMbp = false, hvVga = false;

    try {
        skuList.forEach((sku) => {
            if (sku === "atv") 
                atvCount++;
            else if (sku === "ipd") 
                ipdCount++; 
            else if (sku === "mbp")
                hvMbp = true;
            else if (sku === "vga")
                hvVga = true
            //  loop the data file for getting the info of SKUs, e.g. price
            SKUProductList.forEach((data) => {
                if (sku === data.sku) {
                    //  3 for 2 deal on Apple TVs
                    if (sku === "atv" && atvCount >= 3) {
                        product = {
                            "sku": data.sku,
                            "name": data.name,
                            "price": 0
                        } 
                    } else {
                        product = {
                            "sku": data.sku,
                            "name": data.name,
                            "price": data.price
                        }
                    }
                    productList.push(product);
                }
            })
        });
    } catch (error) {
        console.log("SKUs forEach Error: ", error);
    }

    try {
        //  iPad buys more than 4, the price of it will drop to $499.99 instand of $599.99
        if (ipdCount >= 4) {
            productList.forEach((product) => {
                if (product.sku === "ipd") {
                    product.price = 499.99
                }
            })
        }
    } catch (error) {
        console.log("Super iPad discount Error: ", error);
    }

    try {
        //  VGA adapter free of charge with every MacBook Pro sold
        if (hvMbp === true && hvVga) {
            productList.forEach((product) => {
                if (product.sku === "vga") {
                    product.price = 0
                }
            })
        }
    } catch (error) {
        console.log("VGA adapter free of charge Error: ", error);
    }

    try {
        //  return the price amount from the a callback func - checkout
        return callback(productList);
    } catch (error) {
        return error;
    }
   
};

//  the function of checkout is planned to complete the order
var checkout = function(request){
    // console.log("-------- CHECKOUT --------")
    var totalAmount = 0;

    request.forEach((sku) => {
        totalAmount += sku.price
    })

    return totalAmount.toFixed(2)
};

//  Test Case with example scenarios
var test = function(){
    var testCase1 = (discount("atv, atv, atv, vga", checkout) === "249.00") ? true : false;
    var testCase2 = (discount("atv, ipd, ipd, atv, ipd, ipd, ipd", checkout) === "2718.95") ? true : false;
    var testCase3 = (discount("mbp, vga, ipd", checkout) === "1949.98") ? true : false;
    
    console.log("Test Case 1: ", testCase1, " // atv * 3 AND vga * 1 (249.00)")
    console.log("Test Case 2: ", testCase2, " // atv * 2 AND ipd * 5 (2718.95)");
    console.log("Test Case 3: ", testCase3, " // mbp, vga, ipd (1949.98)");
}

//  this function is used to get the input of the sku list
rl.question('SKUs Scanned: ', (skuList) => {
    //  get the data from the console input
    var price = discount(skuList, checkout);

    console.log(`Total expected: $${price}`);

    rl.close();
}, test());